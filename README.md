Basic syntax highlighting for openHAB in Emacs

Clone this repository to `~/.emacs.d/openhab2`

Append the following to `~/.emacs`
~~~~
  (add-to-list 'load-path "~/.emacs.d/openhab2")
  (load "openhab2-rules.el")
  (load "openhab2-items.el")
~~~~
