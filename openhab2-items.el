(defvar openhab2-items-hook nil)

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.items\\'" . openhab2-items))

(defvar openhab2-items-font-lock-keywords
  (list
	;; (eval-when-compile (concat "\\_<" (regexp-opt '("Color" "Contact" "DateTime" "Dimmer" "Group" "Image" "Location" "Player" "Rollershutter" "String" "Switch" )) "\\_>"))
	'("\\_<\\(?:Co\\(?:lor\\|ntact\\)\\|D\\(?:ateTime\\|immer\\)\\|Group\\|Image\\|Location\\|Player\\|Rollershutter\\|S\\(?:tring\\|witch\\)\\)\\_>" . font-lock-type-face)
	;; (eval-when-compile (concat "\\_<Number:" (regexp-opt '("Acceleration" "AmountOfSubstance" "Angle" "Area" "ArealDensity" "CatalyticActivity" "DataAmount" "DataTransferRate" "Density" "Dimensionless" "ElectricCapacitance" "ElectricCharge" "ElectricConductance" "ElectricConductivity" "ElectricCurrent" "ElectricInductance" "ElectricPotential" "ElectricResistance" "Energy" "Force" "Frequency" "Illuminance" "Intensity" "Length" "LuminousFlux" "LuminousIntensity" "MagneticFlux" "MagneticFluxDensity" "Mass" "Power" "Pressure" "RadiationDoseAbsorbed" "RadiationDoseEffective" "Radioactivity" "SolidAngle" "Speed" "Temperature" "Time" "Volume" "VolumetricFlowRate" )) "\\_>"))
	'("\\_<Number\\(?::\\(?:A\\(?:cceleration\\|mountOfSubstance\\|ngle\\|rea\\(?:lDensity\\)?\\)\\|CatalyticActivity\\|D\\(?:ata\\(?:Amount\\|TransferRate\\)\\|ensity\\|imensionless\\)\\|E\\(?:lectric\\(?:C\\(?:apacitance\\|harge\\|onduct\\(?:ance\\|ivity\\)\\|urrent\\)\\|Inductance\\|Potential\\|Resistance\\)\\|nergy\\)\\|F\\(?:orce\\|requency\\)\\|I\\(?:lluminance\\|ntensity\\)\\|L\\(?:ength\\|uminous\\(?:Flux\\|Intensity\\)\\)\\|Ma\\(?:gneticFlux\\(?:Density\\)?\\|ss\\)\\|P\\(?:ower\\|ressure\\)\\|Radi\\(?:ationDose\\(?:Absorbed\\|Effective\\)\\|oactivity\\)\\|S\\(?:olidAngle\\|peed\\)\\|\\(?:T\\(?:emperatur\\|im\\)\\|Volum\\(?:etricFlowRat\\)?\\)e\\)\\)?\\_>" . font-lock-type-face)
	;; (eval-when-compile (concat "\\_<" (regexp-opt '("ON" "OFF" "OPEN" "CLOSED" "SET" "UNSET" "NULL" "nil" "null")) "\\_>"))
	'("\\_<\\(?:CLOSED\\|NULL\\|O\\(?:FF\\|\\(?:PE\\)?N\\)\\|SET\\|UNSET\\|n\\(?:\\(?:i\\|ul\\)l\\)\\)\\_>" . font-lock-constant-face)
	'("\\_<\\([[:digit:]]+\\(?:\\\.[[:digit:]]*\\)?\\(?:|°?[[:word:]]+\\)?\\)\\_>" . font-lock-constant-face)
	'("(\\(\\\w+\\)\\(?:,\\(\\\w+\\)\\)*)" . font-lock-function-name-face)
	'("<\\(\\\w+\\)>" . font-lock-builtin-face)
	)
  "openHAB Items keywords")

(defvar openhab2-items-syntax-table
  (let ((openhab2-items-syntax-table (make-syntax-table)))
	
    ; This is added so entity names with underscores can be more easily parsed
	(modify-syntax-entry ?_ "w" openhab2-items-syntax-table)
	
	; Comment styles are same as C++
	(modify-syntax-entry ?/ ". 124b" openhab2-items-syntax-table)
	(modify-syntax-entry ?* ". 23" openhab2-items-syntax-table)
	(modify-syntax-entry ?\n "> b" openhab2-items-syntax-table)
	openhab2-items-syntax-table)
  "Syntax table for openHAB Items")


(defun openhab2-items ()
  (interactive)
  (kill-all-local-variables)
  (set-syntax-table openhab2-items-syntax-table)
  ;; Set up font-lock
  (set (make-local-variable 'font-lock-defaults) '(openhab2-items-font-lock-keywords))
  (setq major-mode 'openhab2-items)
  (setq mode-name "openHAB Items")
  (run-hooks 'openhab2-items-hook))

(provide 'openhab2-items)
