(defvar openhab2-rules-hook nil)

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.rules\\'" . openhab2-rules))

(defvar openhab2-rules-font-lock-keywords
  (list
	;;	(eval-when-compile (concat "\\_<" (regexp-opt '("case" "else" "if" "import" "or" "new" "switch" "var" "val" "as")) "\\_>"))
   '("\\_<\\(?:as\\|case\\|else\\|i\\(?:f\\|mport\\)\\|new\\|or\\|switch\\|va[lr]\\)\\_>" . font-lock-keyword-face)
	;;	(eval-when-compile (concat "\\_<" (regexp-opt '("getActions" "logInfo" "logDebug" "logWarn" "logError" "postUpdate" "println" "sendCommand" "say" "transform" "now")) "\\_>"))
	'("\\_<\\(getActions\\|log\\(?:Info\\|Debug\\|Warn\\|Error\\)\\|p\\(?:ostUpdate\\|rintln\\)\\|s\\(?:ay\\|endCommand\\)\\|transform\\)\\_>" . font-lock-builtin-face)
	'("\\_<\\(rule\\|when\\|then\\|end\\)\\_>" . font-lock-preprocessor-face)
	;; (eval-when-compile (concat "\\_<" (regexp-opt '("Item" "System" "String" "DateTime" "QuantityType" "Number" "DecimalType" "Timer" "Time")) "\\_>"))
	'("\\_<\\(?:D\\(?:\\(?:ateTim\\|ecimalTyp\\)e\\)\\|Item\\|Number\\|QuantityType\\|S\\(?:tring\\|ystem\\)\\|Timer?\\)\\_>" . font-lock-type-face)
	;; (eval-when-compile (concat "\\_<" (regexp-opt '("received" "is" "to" "from" "update" "changed" "command" "cron" "started" "triggered")) "\\_>"))
	'("\\_<\\(?:c\\(?:hanged\\|ommand\\|ron\\)\\|from\\|is\\|received\\|started\\|t\\(?:o\\|riggered\\)\\|update\\)\\_>" . font-lock-function-name-face)
	;; (eval-when-compile (concat "\\_<" (regexp-opt '("ON" "OFF" "OPEN" "CLOSED" "SET" "UNSET" "NULL" "nil" "null")) "\\_>"))
	'("\\_<\\(?:CLOSED\\|NULL\\|O\\(?:FF\\|\\(?:PE\\)?N\\)\\|SET\\|UNSET\\|n\\(?:\\(?:i\\|ul\\)l\\)\\)\\_>" . font-lock-constant-face)
	'("\\_<\\([[:digit:]]+\\(?:\\\.[[:digit:]]*\\)?\\(?:|°?[[:word:]]\\)?\\)\\_>" . font-lock-constant-face)
	)
  "openHAB Rules keywords")

(defvar openhab-rules-syntax-table
  (let ((openhab-rules-syntax-table (make-syntax-table)))
	
    ; This is added so entity names with underscores can be more easily parsed
	(modify-syntax-entry ?_ "w" openhab-rules-syntax-table)
	
	; Comment styles are same as C++
	(modify-syntax-entry ?/ ". 124b" openhab-rules-syntax-table)
	(modify-syntax-entry ?* ". 23" openhab-rules-syntax-table)
	(modify-syntax-entry ?\n "> b" openhab-rules-syntax-table)
	openhab-rules-syntax-table)
  "Syntax table for openHAB Rules")


(defun openhab2-rules ()
  (interactive)
  (kill-all-local-variables)
  (set-syntax-table openhab-rules-syntax-table)
  ;; Set up font-lock
  (set (make-local-variable 'font-lock-defaults) '(openhab2-rules-font-lock-keywords))
  (setq major-mode 'openhab2-rules)
  (setq mode-name "openHAB Rules")
  (run-hooks 'openhab2-rules-hook))

(provide 'openhab2-rules)
